module Main
import TBoolean
import TString

data SwenuBoolean = SwenuTrue | SwenuFalse

swenuBooleanToString : SwenuBoolean -> String
swenuBooleanToString SwenuTrue = "true"
swenuBooleanToString SwenuFalse = "false"

ifThenElse : SwenuBoolean -> (t: Lazy a) -> (f: Lazy a) -> a
ifThenElse SwenuTrue t _ = t
ifThenElse SwenuFalse _ f = f

swenuOr : SwenuBoolean -> SwenuBoolean -> SwenuBoolean
swenuOr SwenuTrue _ = SwenuTrue
swenuOr _ SwenuTrue = SwenuTrue
swenuOr _ _ = SwenuFalse

swenuAnd : SwenuBoolean -> SwenuBoolean -> SwenuBoolean
swenuAnd SwenuTrue SwenuTrue = SwenuTrue
swenuAnd _ _ = SwenuFalse

mikeIsNotCool : SwenuBoolean
mikeIsNotCool = SwenuFalse

swenuLikesPiano : SwenuBoolean
swenuLikesPiano = SwenuTrue

outcome : SwenuBoolean
outcome = swenuOr swenuLikesPiano swenuLikesPiano

message : String
message = ifThenElse outcome "yay swenu" "nooooo swenu"

printMessage : IO ()
printMessage = putStrLn message

main : IO ()
main = do
    emptyLine
    printMessage
    emptyLine
    writeString "NOT"
    writeBoolean (not TTrue)
    writeBoolean (not TFalse)
    emptyLine
    writeString "ORs"
    writeBoolean (TTrue `or` TTrue)
    writeBoolean (TTrue `or` TFalse)
    writeBoolean (TFalse `or` TTrue)
    writeBoolean (TFalse `or` TFalse)
    emptyLine
    writeString "ANDs"
    writeBoolean (TTrue `and` TTrue)
    writeBoolean (TTrue `and` TFalse)
    writeBoolean (TFalse `and` TTrue)
    writeBoolean (TFalse `and` TFalse)

seven : Integer
seven = 