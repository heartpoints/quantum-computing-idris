module TString

export 
writeString : String -> IO ()
writeString = putStrLn

export
emptyLine : IO ()
emptyLine = writeString ""