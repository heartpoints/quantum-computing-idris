quantum-computing
=================

# Getting Started

## Prerequisites

Before proceeding, ensure the following software is installed and accessible to your development environment:

1. Docker
2. Git
3. VS Code

## Clone Repository

`git clone https://gitlab.com/heartpoints/quantum-computing-idris.git`

## Open in VS Code

VS Code will notice the `./.devcontainer` folder and prompt to open in devcontainer. Choose yes. This will prepare everything needed for your development environment.

## Verify Idris works

Open a terminal. VS Code should open this within the container. From here, run `idris2` to confirm that the compiler is installed and working.

## Verify VS Code Plugin works

open up `src/TypeDD.idr` and, it may take a while first time, but eventually you can confirm that Go To Definition works, including for built-in types, and that holes have code actions to suggest implementations, etc.

